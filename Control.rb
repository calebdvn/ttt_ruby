class Control
  @@combos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]

  def self.new_board
    Array.new(9,0)
  end

  def self.free_tiles(board)
    free_tiles_list = []; #start out the free square list as empty
    board.each_with_index{|tile,idx|
      if tile==0 then free_tiles_list.push(idx); end #add to the list any empty tile indices
    }
    free_tiles_list #return the free square list
  end

  def self.game_over?(board)
    if(free_tiles(board).length == 0) then return true; end #if the board is filled up, return true
    @@combos.each do |combo|
      if (board.values_at(*combo).sum().abs == 3) then return true; end #if someone wins return true
    end
    false
  end

  def self.winner(board) #this function assumes the game is over
    @@combos.each do |combo|
      if (board.values_at(*combo).sum().abs == 3) then return board[combo[0]]; end #if someone wins return true
    end
    return 0 #if the board is filled up, return true
  end

  def self.get_player(board)
    (free_tiles(board).length%2)*2-1
  end

  # def self.human_make_move(board, tile=-1) #If no input is given, player inputs
  #   while (tile < 0 || tile > 8 || board[tile] != 0) do
  #     tile = gets.to_i
  #   end
  #   board[tile] = Control.get_player(board)
  # end

  def self.rem_move(board, tile)
    board[tile]=0
  end

  def self.make_move(board, tile)
    board[tile] = Control.get_player(board)
  end

  def self.ai(board_in)
    board = board_in.clone
    free_tiles = Control.free_tiles(board)
    if !Control.game_over?(board)
      results = []
      free_tiles.each do |tile|
        Control.make_move(board,tile)
        results.push [tile, ai(board)[1]]
        Control.rem_move(board,tile)
      end
    else
      return [nil, winner(board)*(1.0+(Control.free_tiles(board).length/10.0))] #multiplication for selecting the shortest win route
    end
    return Control.best_result(results,Control.get_player(board))
  end

  def self.best_result(results, player) #results of the form [tile, winner]
    best_result = results[0];
    if player == 1
      results.each{|row| if (row[1] > best_result[1]) then best_result = row; end}
    else
      results.each{|row| if (row[1] < best_result[1]) then best_result = row; end}
    end
    best_result
  end

end
