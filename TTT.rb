require 'sinatra'
require_relative 'Control.rb'
set :port, 25565
set :bind, '0.0.0.0'

$tile_imgs = []
$bottom_button_img = 'pictures/Play_As_O.png'
$background = 'pictures/Background.png'
$board = Array.new(9,0)
$game_state = 0  #0 = not playing; 1 = playing; 2 = game over

def get_image(board) #must be called before make_move
  return 'pictures/X.png' if Control.get_player(board) == 1
  return 'pictures/O.png'
end

get '/' do
  if $tile_imgs == []
    reset_game
  end
  erb :TTT_page
end

(0...9).each do |i|
  get '/button'+i.to_s do
    if $game_state < 2
      $game_state = 1
      $bottom_button_img = 'pictures/New_Game.png'
      click_tile(i)
      if Control.game_over?($board)
        redirect '/gameover'
      else
        follow_up_move
        if Control.game_over?($board)
          redirect '/gameover'
        else
          redirect '/'
        end
      end
    else
      redirect '/'
    end
  end
end

def click_tile(tile)
  if $board[tile]==0
    $tile_imgs[tile] = get_image($board)
    Control.make_move($board, tile)
  end
end

def follow_up_move
  tile = Control.ai($board)[0]
  $tile_imgs[tile] = get_image($board)
  Control.make_move($board, tile)
end

get '/gameover' do
  $game_state = 2
  case Control.winner($board)
  when 1
    $display_message = 'X Wins!'
  when -1
    $display_message = 'O wins!'
  when 0
    $display_message = 'Cats game.'
  else
    $display_message = 'How did you do that?'
  end
  redirect '/'
end

get '/bottom_button' do
  if $game_state == 1 || $game_state == 2
    reset_game
  else
    click_tile(4)
    $bottom_button_img = 'pictures/New_Game.png'
  end
  redirect '/'
end

def reset_game
  $board = Array.new(9,0)
  $tile_imgs = Array.new(9,'pictures/Blank.png')
  $bottom_button_img = 'pictures/Play_As_O.png'
  $game_state = 0
  $display_message = ''
end
