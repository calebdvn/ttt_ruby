require "minitest/autorun"
require_relative "TTT.rb"

class TTT_test < Minitest::Test
  def test_get_image
    board = Array.new(9,0)
    image_path = get_image(board)
    assert_equal(image_path, 'pictures/X.png')
    board = [
               1,  1, 0,
              -1, -1, 0,
               1, -1, 1
            ]
    image_path = get_image(board)
    assert_equal(image_path, 'pictures/O.png')
  end

  def test_click_tile_update_board
    $tile_imgs = Array.new(9,'pictures/Blank.png')
    $board = Array.new(9,0)
    expected_board = Array.new(9,0)


    tile = 4
    click_tile(tile)
    expected_board[tile] = 1
    assert_equal(expected_board,$board)
    assert_equal('pictures/X.png',$tile_imgs[tile])

    tile = 2
    click_tile(tile)
    expected_board[tile] = -1
    assert_equal(expected_board,$board)
    assert_equal('pictures/O.png',$tile_imgs[tile])

    tile = 2
    click_tile(tile)
    assert_equal(expected_board,$board)
    assert_equal('pictures/O.png',$tile_imgs[tile])
  end

  def test_follow_up_move
    $board =
    [
     -1, -1,  0,
      1,  1,  0,
      0,  0,  0
    ]
    ai_results = Control.ai($board)
    follow_up_move
    expected_board =
    [
     -1, -1,  0,
      1,  1,  1,
      0,  0,  0
    ]
    assert_equal(expected_board,$board, ai_results.to_s)

  end

  def test_reset_game
    $board =
    [
       1, 1,-1,
      -1,-1, 1,
       1,-1, 1
    ]
    $tile_imgs = ['to_be_reset','blah']
    reset_game
    assert_equal(Array.new(9,0),$board)
    assert_equal($tile_imgs,Array.new(9,'pictures/Blank.png'))
  end
end
