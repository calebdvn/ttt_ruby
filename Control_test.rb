require "minitest/autorun"
require_relative "Control.rb"

class Control_test < Minitest::Test

  def test_new_board
    board = Control.new_board
    assert_equal(board, Array.new(9,0))
  end

  def test_free_tiles
    board = Control.new_board
    assert_equal(Control.free_tiles(board),(0...9).to_a)
    board=[0,0,0, 0,0,0, 1,-1,1]
    assert_equal(Control.free_tiles(board),[0,1,2,3,4,5])
    board=[0,0,-1, 1,-1,1, -1,1,1]
    assert_equal(Control.free_tiles(board),[0,1])
    board=[0,0,0, 0,-1,-1, 1,1,1]
    assert_equal(Control.free_tiles(board),[0,1,2,3])
    board=[1,1,-1, -1,-1, 1, 1,1,-1]
    assert_equal(Control.free_tiles(board),[])
    board=[1,1,-1, -1,-1, 1, 1,0,-1]
    assert_equal(Control.free_tiles(board),[7])
  end

  def test_game_over?
    board = Control.new_board
    assert_equal(Control.game_over?(board),false)
    board=[0,0,0, 0,0,0, 1,-1,1]
    assert_equal(Control.game_over?(board),false)
    board=[0,0,-1, 1,-1,1, -1,1,1]
    assert_equal(Control.game_over?(board),true)
    board=[0,0,0, 0,-1,-1, 1,1,1]
    assert_equal(Control.game_over?(board),true)
    board=[1,1,-1, -1,-1, 1, 1,1,-1]
    assert_equal(Control.game_over?(board),true)
  end

  def test_winner
    board = Control.new_board
    assert_equal(Control.winner(board),0)
    board=[0,0,0, 0,0,0, 1,-1,1]
    assert_equal(Control.winner(board),0)
    board=[0,0,-1, 1,-1,1, -1,1,1]
    assert_equal(Control.winner(board),-1)
    board=[0,0,0, 0,-1,-1, 1,1,1]
    assert_equal(Control.winner(board),1)
    board=[1,1,-1, -1,-1, 1, 1,1,-1]
    assert_equal(Control.winner(board),0)
  end

  def get_player
    board = Control.new_board
    assert_equal(Control.get_player(board),1)
    board=[0,0,0, 0,0,0, 1,-1,1]
    assert_equal(Control.get_player(board),-1)
    board=[1,0,-1, -1,-1, 1, 1,1,-1]
    assert_equal(Control.get_player(board),1)
  end

  # def test_human_make_move
  #   board =
  #   [
  #     -1,  0,  0,
  #      1, -1,  0,
  #     -1,  1,  1
  #   ]
  #
  #   compare_board = board.clone
  #
  #   Control.human_make_move(board,2)
  #   compare_board[2]=1;
  #   assert_equal(board,compare_board)
  #
  #   Control.human_make_move(board,5)
  #   compare_board[5]=-1;
  #   assert_equal(board,compare_board)
  #
  # end

  def test_rem_move
    board =
    [
      -1,  0,  0,
       1, -1,  0,
      -1,  1,  1
    ]
    expected_board =
    [
      -1,  0,  0,
       1, -1,  0,
       0,  1,  1
    ]
    Control.rem_move(board,6)
    assert_equal(expected_board,board)
  end

  def test_make_move
    board =
    [
      -1,  0,  0,
       1, -1,  0,
      -1,  1,  1
    ]

    compare_board = board.clone

    Control.make_move(board,2)
    compare_board[2]=1;
    assert_equal(board,compare_board)

    Control.make_move(board,5)
    compare_board[5]=-1;
    assert_equal(board,compare_board)

  end

  def test_ai
    board =
    [
      -1,  1,  0,
       1, -1,  0,
      -1,  1,  1
    ]
    ai_results = Control.ai(board)
    expected_ai_results = [2, -1.1]
    assert_equal(expected_ai_results, ai_results)

    board =
    [
      -1,  0,  0,
       1, -1,  0,
      -1,  1,  1
    ]
    ai_results = Control.ai(board);
    expected_ai_results = [2, 0]
    assert_equal(expected_ai_results, ai_results)

    board =
    [
      1,  0,  0,
     -1,  1,  0,
      0,  0, -1
    ]
    ai_results = Control.ai(board);
    expected_ai_results = [[2,1.2],[1,1.2]]
    assert_includes(expected_ai_results, ai_results)

    board =
    [
     -1,  0,  0,
      0,  1, -1,
      0,  0,  1
    ]
    ai_results = Control.ai(board);
    expected_ai_results = [[6,1.2],[7,1.2]]
    assert_includes(expected_ai_results, ai_results)

    #The ai should select the fastest solution. If it does not select the
    #shortest solution, then it will place a mark in board[2], but the shortest
    #winning solution would be board[5], in the following board
    board =
    [
     -1, -1,  0,
      1,  1,  0,
      0,  0,  0
    ]
    ai_results = Control.ai(board);
    expected_ai_results = [5, 1.4]
    assert_equal(ai_results, expected_ai_results)

  end

 #results are of the form [tile, winner]
  def test_best_result
    results = [[2,0],[3,1],[4,-1],[5,0],[6,1]];
    assert_equal(Control.best_result(results, 1),[3,1])
    assert_equal(Control.best_result(results, -1),[4,-1])
    results = [[2,0],[3,1],[4,01],[5,0],[6,1]];
    assert_equal([2,0],Control.best_result(results, -1))
  end
end
